//
//  ContentView.swift
//  metalTrailsMacOS
//
//  Created by Raul on 1/8/24.
//

import SwiftUI
import MetalKit

struct TrailPoint {
    var position: SIMD4<Float> // Match the float4 type
    var color: SIMD4<Float> // Match the float4 type
}

struct MetalView: NSViewRepresentable {
    static var defaultLibrary: MTLLibrary = {
        guard let library = MTLCreateSystemDefaultDevice()?.makeDefaultLibrary() else {
            fatalError("Failed to create default Metal library")
        }
        return library
    }()

    // Set up rendering pipeline
    static var pipelineState: MTLRenderPipelineState = {
        let pipelineDescriptor = MTLRenderPipelineDescriptor()
        pipelineDescriptor.vertexFunction = defaultLibrary.makeFunction(name: "vertexShader")
        pipelineDescriptor.fragmentFunction = defaultLibrary.makeFunction(name: "fragmentShader")
        pipelineDescriptor.colorAttachments[0].pixelFormat = .bgra8Unorm

        guard let pipelineState = try? MTLCreateSystemDefaultDevice()?.makeRenderPipelineState(descriptor: pipelineDescriptor) else {
            fatalError("Failed to create render pipeline state")
        }

        return pipelineState
    }()

    class Coordinator: NSObject, MTKViewDelegate {
        var trailBuffer: MTLBuffer?
        let trailLength = 30  // Number of points in the trail
        var frameCount: UInt32 = 0
        
        
        
        init(_ parent: MetalView) {
            super.init()
            
            guard let device = MTLCreateSystemDefaultDevice() else {
                fatalError("Metal is not supported on this device")
            }
            
            let initialTrailPoints = [TrailPoint](repeating: TrailPoint(position: SIMD4<Float>(0, 0, 0, 1), color: SIMD4<Float>(1, 0, 0, 1)), count: trailLength)
            trailBuffer = device.makeBuffer(bytes: initialTrailPoints, length: MemoryLayout<TrailPoint>.stride * trailLength, options: [])
        }
        
        func updateTrailBuffer(device: MTLDevice) {
            var points = [TrailPoint]()
            if let buffer = trailBuffer {
                let contents = buffer.contents().bindMemory(to: TrailPoint.self, capacity: trailLength)
                points = Array(UnsafeBufferPointer(start: contents, count: trailLength))
                
                for i in (1..<trailLength).reversed() {
                    points[i] = points[i - 1]
                    points[i].position.y -= 0.007 // Slightly decrease the y position for each point in the trail
                }
                
                // Generate a new color for the leading point
                let newColor: SIMD4<Float> = generateRandomColor(frameCount: frameCount)
                let yOffset = Float(frameCount) * 0.001
                points[0] = TrailPoint(position: SIMD4<Float>(0, yOffset, 0, 1), color: newColor)
                
                memcpy(contents, points, MemoryLayout<TrailPoint>.stride * trailLength)
            }
        }
        
        func generateRandomColor(frameCount: UInt32) -> SIMD4<Float> {
            var seed = frameCount &* 1928723
            seed ^= seed >> 16
            seed = seed &* 0x85ebca6b
            seed ^= seed >> 13
            seed = seed &* 0xc2b2ae35
            seed ^= seed >> 16
            
            let red = Float((seed >> 16) & 0xFF) / 255.0
            let green = Float((seed >> 8) & 0xFF) / 255.0
            let blue = Float(seed & 0xFF) / 255.0
            return SIMD4<Float>(red, green, blue, 1.0) // Alpha is always 1.0 for full opacity
        }

        
        func mtkView(_ view: MTKView, drawableSizeWillChange size: CGSize) {
            // Handle view size change if needed
        }
        
        func draw(in view: MTKView) {
            frameCount += 1  // Increment frame count
            
            updateTrailBuffer(device: view.device!)
            
            let vertices: [SIMD3<Float>] = [
                SIMD3<Float>(0, 0, 0) // Center point
            ]
            
            let vertexBuffer = view.device?.makeBuffer(bytes: vertices, length: vertices.count * MemoryLayout<SIMD3<Float>>.stride, options: [])
            
            guard let commandBuffer = view.device?.makeCommandQueue()?.makeCommandBuffer(),
                  let renderPassDescriptor = view.currentRenderPassDescriptor,
                  let renderEncoder = commandBuffer.makeRenderCommandEncoder(descriptor: renderPassDescriptor) else {
                return
            }
            
            renderEncoder.setVertexBytes(&frameCount, length: MemoryLayout<UInt32>.size, index: 1)
            renderEncoder.setFragmentBytes(&frameCount, length: MemoryLayout<UInt32>.size, index: 0)
            
            if let trailBuffer = trailBuffer {
                renderEncoder.setVertexBuffer(trailBuffer, offset: 0, index: 2)
                renderEncoder.setFragmentBuffer(trailBuffer, offset: 0, index: 2)
            }
            
            renderEncoder.setRenderPipelineState(MetalView.pipelineState)
            renderEncoder.setVertexBuffer(vertexBuffer, offset: 0, index: 0)
            renderEncoder.drawPrimitives(type: .point, vertexStart: 0, vertexCount: trailLength)
            
            renderEncoder.endEncoding()
            
            if let drawable = view.currentDrawable {
                commandBuffer.present(drawable)
            }
            
            commandBuffer.commit()
        }
    }

    func makeCoordinator() -> Coordinator {
        Coordinator(self)
    }

    func makeNSView(context: NSViewRepresentableContext<MetalView>) -> MTKView {
        let metalView = MTKView()
        metalView.device = MTLCreateSystemDefaultDevice()
        metalView.delegate = context.coordinator
        metalView.clearColor = MTLClearColorMake(0, 0, 0, 1) // Set the default background color to black
        metalView.preferredFramesPerSecond = 60
        
        metalView.enableSetNeedsDisplay = true // Enables the view to redraw itself when setNeedsDisplay() is called.
        // This can be used for manual control of the redraw cycle.
        
        metalView.framebufferOnly = false // Indicates that the contents of the framebuffer will be read by the CPU.
        // Setting to false allows for more flexibility, such as performing readback operations.
        
        
        metalView.drawableSize = metalView.frame.size // Sets the drawable size of the view to match its frame size.
        // This ensures that the rendering resolution matches the view size.
        
        
        
        metalView.isPaused = false // This line ensures that the MTKView's internal loop is active.
        // By default, MTKView might pause its loop to conserve resources.
        // Setting isPaused to false keeps the rendering loop running,
        // allowing continuous rendering of frames, which is essential for animations or any dynamic content.
        

        
        
        return metalView
    }

    func updateNSView(_ nsView: MTKView, context: NSViewRepresentableContext<MetalView>) {
        // Update Metal view if needed
    }
}



struct ContentView: View {
    var body: some View {
        VStack {
            MetalView() // Embedding the MetalView within the SwiftUI view hierarchy.
            // This is where the Metal content will be displayed.
            
            
        }
    }
    
}
