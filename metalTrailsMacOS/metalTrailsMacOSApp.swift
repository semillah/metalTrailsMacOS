//
//  metalTrailsMacOSApp.swift
//  metalTrailsMacOS
//
//  Created by Raul on 1/8/24.
//

import SwiftUI

@main
struct metalTrailsMacOSApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
