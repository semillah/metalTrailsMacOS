#include <metal_stdlib>
using namespace metal;

struct TrailPoint {
    float4 position ; // x, y, z, and w
    float4 color;    // r, g, b, and a
};

struct Vertex {
    float3 position [[attribute(0)]];
};

struct VertexOut {
    float4 position [[position]];
    float pointSize [[point_size]]; // Added to control the size of the point
    uint index; // Pass the index to the fragment shader
};

vertex VertexOut vertexShader(const device Vertex* vertexArray [[buffer(0)]],
                              device TrailPoint* trailPoints [[buffer(2)]],
                              unsigned int vid [[vertex_id]],
                              constant uint &frameCount [[buffer(1)]]) {
    VertexOut output;
    
    // Determine the index of the point in the trail
    uint index = vid % 30; // Updated for trail length of 100

    // Use the trail point data to position the points
    output.position = trailPoints[index].position ;
    output.pointSize = max(1.0, 6.0 - (index * .2)); // Adjust to ensure smaller point sizes
    output.index = index ; // Pass the index to the fragment shader

    return output;
}


fragment float4 fragmentShader(VertexOut vertex_out [[stage_in]],
                               device TrailPoint* trailPoints [[buffer(2)]]) {
    uint index = vertex_out.index ; // Use the index from the vertex shader
    
    float4 color = trailPoints[index].color; // Directly use the trail point color
    // Apply a fading effect based on the point's index
    float opacity = 1.0 - (index * 0.05); // Gradually decrease the opacity
    color.a *= opacity;
    
    return color; // Return the color with applied opacity
}

